import cv2


# Creates a VideoCapture object from the 'Nemo.mp4' file
nemo = cv2.VideoCapture('Nemo.mp4')

# HSV color codes
LOWER = (80, 140, 200)			# Lower orange color code
UPPER = (140, 255, 255)			# Upper orange color code
WHITE = (0, 0, 220)				# Lower white color code
DARK = (80, 80, 255)			# Upper white color code


# Read until the video is over
while nemo.isOpened():
	# Read the frame
	ret, frame = nemo.read()

	# If the frame exists
	if ret:
		# Convert the frame from RGB to HSV
		hsvframe = cv2.cvtColor(frame, cv2.COLOR_RGB2HSV)

		# Create a color segmentation mask on the frame
		orange_mask = cv2.inRange(hsvframe, LOWER, UPPER)
		white_mask = cv2.inRange(hsvframe, WHITE, DARK)
		mask = orange_mask + white_mask

		# Apply the mask over the frame
		result = cv2.bitwise_and(frame, frame, mask=mask)

		# Resize the video display window
		result = cv2.resize(result, (480, 270))
		frame = cv2.resize(frame, (480, 270))

		# Display the original and the color segmented videos
		cv2.imshow('Color segmented video', result)
		cv2.imshow('Original video', frame)

		# Break the loop if 'q' is pressed
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

	# Break the loop if the video is over
	else:
		break

# Release the video capture object
nemo.release()
# Close all frames
cv2.destroyAllWindows()
